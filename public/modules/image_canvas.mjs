"use strict";
import { LayerCanvas } from "./layer_canvas.mjs";
import { MouseDrag } from "./mousedrag.mjs";

export class ImageCanvas {
  #target = null;
  #wrap = document.createElement("div");
  #image = new Image();
  #canvases;
  #contexts;
  #external_callback = {load : null, ope : null};
  #zoom = 1.0;
  #scroll = {x : 0.0, y : 0.0};
  #drag = new MouseDrag();

  constructor (div, resolution, init_image = null) {
    this.#target = div;
    this.#target.appendChild(this.#wrap);
    this.#canvases = new LayerCanvas(this.#wrap, resolution[0], resolution[1], 2);
    this.#canvases.addEventListener("mousedown", event => this.#MousedownCallback(event));
    this.#canvases.addEventListener("mousemove", event => this.#MousemoveCallback(event));
    this.#canvases.addEventListener("mouseup", event => this.#MouseupCallback(event));
    this.#canvases.addEventListener("mouseleave", event => this.#MouseupCallback(event));
    this.#canvases.addEventListener("wheel", event => this.#WheelCallback(event));
    this.#contexts = this.#canvases.GetContexts("2d");
    this.#DrawGrid();
    this.#image.addEventListener("load", () => this.#LoadImageCallback());
    if (init_image != null) this.#image.src = init_image;
  }

  #DrawGrid() {
    const grid_size = 8;
    const grid_x = Math.floor(this.#canvases.GetWidth() / 8);
    const grid_y = Math.floor(this.#canvases.GetHeight() / 8);
    const grid_len = grid_x * grid_y;
    this.#contexts[0].fillStyle = "lightgray";
    for (let grid = 0; grid < grid_len; ++grid) {
      const x = (grid % grid_x);
      const y = Math.floor(grid / grid_x);
      const fix = grid_x % 2 == 1 ? 0 :
                  y % 2 == 0 ? 0 :
                  1;
      if (((grid + fix) % 2) == 0) {
        this.#contexts[0].fillRect(x * grid_size, y * grid_size, grid_size, grid_size);
      }
    }
  }

  #CalcImagePosition() {
    const canvas_width = this.#canvases.GetWidth();
    const canvas_height = this.#canvases.GetHeight();
    const scale_x = canvas_width / this.#image.width;
    const scale_y = canvas_height / this.#image.height;
    const pos_x = (scale_x <= scale_y ? 0 : Math.abs(this.#image.width * scale_y - canvas_width) * 0.5) + this.#scroll.x;
    const pos_y = (scale_y <= scale_x ? 0 : Math.abs(this.#image.height * scale_x - canvas_height) * 0.5) + this.#scroll.y; 
    const scale = Math.min(scale_x, scale_y) * this.#zoom;
    const size_x = this.#image.width * scale;
    const size_y = this.#image.height * scale;
    return {pos : { x : pos_x, y : pos_y}, size : {x : size_x, y : size_y}};
  }

  #ResetTranspose() {
    this.#zoom = 1.0;
    this.#scroll.x = 0.0;
    this.#scroll.y = 0.0;
  }
  
  #DrawImage(property) {
    this.#contexts[1].clearRect(0, 0, this.#canvases.GetWidth(), this.#canvases.GetHeight());
    this.#contexts[1].drawImage(this.#image, property.pos.x, property.pos.y, property.size.x, property.size.y);
  }

  #LoadImageCallback() {
    this.#ResetTranspose();
    this.#DrawImage(this.#CalcImagePosition());
    if (this.#external_callback.load != null) this.#external_callback.load(this.#image);
    if (this.#external_callback.ope != null) this.#CallExternalOperationCallback();
  }

  #CallExternalOperationCallback() {
    const property = this.#CalcImagePosition();
    const fix_zoom = this.#zoom;
    const fix_scroll = {x : this.#scroll.x / property.size.x, y : this.#scroll.y / property.size.y};
    this.#external_callback.ope(fix_zoom, fix_scroll);
  }

  #MousedownCallback(event) {
    if (event.buttons == 1) {
      this.#drag.MouseDown(event.x, event.y, this.#scroll.x, this.#scroll.y);
    } else if (event.buttons == 4) {
      this.#ResetTranspose();
      this.#DrawImage(this.#CalcImagePosition());
      if (this.#external_callback.ope != null) this.#CallExternalOperationCallback();
    }
  }

  #MousemoveCallback(event) {
    if (this.#drag.IsDraging()) {
      const data = this.#drag.GetDistance(event.x, event.y);
      this.#scroll.x = data.buffer.x;
      this.#scroll.y = data.buffer.y;
      this.#DrawImage(this.#CalcImagePosition());
      if (this.#external_callback.ope != null) this.#CallExternalOperationCallback();
    }
  }

  #MouseupCallback(event) {
    this.#drag.MouseUp();
  }

  #WheelCallback(event) {
    event.preventDefault();
    const property_prev = this.#CalcImagePosition();
    this.#zoom = Math.min(Math.max(.125, this.#zoom + event.deltaY * -0.001), 100);
    const property_temp = this.#CalcImagePosition();
    this.#scroll.x -= (property_temp.size.x - property_prev.size.x) / 2.0;
    this.#scroll.y -= (property_temp.size.y - property_prev.size.y) / 2.0;
    const property = this.#CalcImagePosition();
    this.#DrawImage(property);
    if (this.#external_callback.ope != null) this.#CallExternalOperationCallback();
  }

  GetImage() { return this.#image; }

  SetImageSource(image) { this.#image.src = image; }

  SetLoadImageCallback(func) { this.#external_callback.load = func; }

  SetOperationCallback(func) { this.#external_callback.ope = func; }
};