"use strict";

export function Clamp(n, min, max) {
  return Math.min(Math.max(n, min), max);
}

export class LayerCanvas {
  #target;
  #wrap = document.createElement("div");
  #canvases = new Array();

  constructor (target, width, height, layer) {
    this.#target = target;
    this.#wrap.style.position = "relative";
    this.#wrap.style.display = "block";
    this.#wrap.style.width = width + "px";
    this.#wrap.style.height = height + "px";
    this.#wrap.style.border = "2px solid";
    for (let i = 0; i < layer; ++i) {
      const canvas = document.createElement("canvas");
      canvas.width = width;
      canvas.height = height;
      canvas.style.position = "absolute";
      canvas.style.left = 0;
      canvas.style.top = 0;
      canvas.style.border = "none";
      canvas.style.padding = "none";
      canvas.style.margin = "none";
      this.#wrap.appendChild(canvas);
      this.#canvases.push(canvas);
    }
    this.#target.appendChild(this.#wrap);
  }

  Resize(width, height) {
    this.#wrap.style.width = width + "px";
    this.#wrap.style.height = height + "px";
    this.#canvases.forEach(canvas => {
      canvas.width = width;
      canvas.height = height;
    });
  }

  GetWidth() { return this.#canvases[0].width; }

  GetHeight() { return this.#canvases[0].height; }

  GetContexts(id) { return this.#canvases.map(canvas => canvas.getContext(id)); }

  addEventListener(id, func) { this.#wrap.addEventListener(id, func); }

  static GetMouseCoord(event) {
    const rect = event.target.getBoundingClientRect();
    return {
      x: event.x - rect.left,
      y: event.y - rect.top
    };
  }
};

export class MouseDrug {
  #flag = false;
  #down_pos = {x : 0, y : 0};
  #buffer = {x : 0, y : 0};

  MouseDown(pos_x, pos_y, buffer_x, buffer_y) {
    this.#flag = true;
    this.#down_pos.x = pos_x;
    this.#down_pos.y = pos_y;
    this.#buffer.x = buffer_x;
    this.#buffer.y = buffer_y;
  }

  MouseUp() {
    this.#flag = false;
    this.#down_pos.x = 0;
    this.#down_pos.y = 0;
    this.#buffer.x = 0;
    this.#buffer.y = 0;
  }

  GetDistance(pos_x, pos_y) {
    if (this.#flag) {
      const dist = {x : pos_x - this.#down_pos.x, y : pos_y - this.#down_pos.y}
      const buf = {x : this.#buffer.x + dist.x, y : this.#buffer.y + dist.y}
      return {distance : dist, buffer : buf};
    } else {
      return {distance : {x : 0, y : 0}, buffer : {x : 0, y : 0}};
    }
  }

  GetFlag() { return this.#flag; }
}

class MultiRange {
  #target;
  #wrap = document.createElement("div");
  #ranges = new Array();
  #input_callback = null;

  constructor(target, min, max, step, init) {
    this.#target = target;
    for (let i = 0; i < init.length; ++i) {
      const range = document.createElement("input");
      range.type = "range";
      range.style.border = "none";
      range.style.padding = "none";
      range.style.margin = "none";
      range.style.slider
      range.min = min;
      range.max = max;
      range.step = step;
      range.value = init[i];
      range.addEventListener("input", (event) => this.#InputCallback(event));
      this.#wrap.appendChild(range);
      if (i != (init.length - 1)) this.#wrap.appendChild(document.createElement("br"));
      this.#ranges.push(range);
    }
    this.#target.appendChild(this.#wrap);
  }

  #InputCallback(event) {
    this.#ranges.forEach((range, index) => {
      if (event.target === range) {
        if (index != 0) {
          range.value = Math.max(range.value, this.#ranges[index - 1].value);
        }
        if (index != (this.#ranges.length - 1)) {
          range.value = Math.min(range.value, this.#ranges[index + 1].value);
        }
      }
    });
    if (this.#input_callback != null) this.#input_callback();
  }

  SetCallback(func) { this.#input_callback = func; }

  get value() { return this.#ranges.map(range => range.value); }
  GetValues() { return this.value; }
}

export class RangeInputs {
  #target;
  #wrap = document.createElement("div");
  #ranges = new Array();
  #input_callback = null;

  constructor(target, recipe) {
    this.#target = target;
    for (let i = 0; i < recipe.length; ++i) {
      if (Array.isArray(recipe[i].init)) {
        const multi = new MultiRange(this.#wrap, recipe[i].min, recipe[i].max, recipe[i].step, recipe[i].init);
        multi.SetCallback(() => this.#InputCallback());
        this.#ranges.push(multi);
      } else {
        const range = document.createElement("input");
        range.type = "range";
        range.min = recipe[i].min;
        range.max = recipe[i].max;
        range.step = recipe[i].step;
        range.value = recipe[i].init;
        range.addEventListener("input", () => this.#InputCallback());
        this.#wrap.appendChild(range);
        if (i != (recipe.length - 1)) this.#wrap.appendChild(document.createElement("br"));
        this.#ranges.push(range);
      }
    }
    this.#target.appendChild(this.#wrap);
  }

  #InputCallback() { if (this.#input_callback != null) this.#input_callback(); }

  SetCallback(func) { this.#input_callback = func; }

  GetValues() {
    return this.#ranges.map(range => range.value);
  }
}