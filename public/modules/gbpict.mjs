"use strict";
import { ImageCanvas } from "./image_canvas.mjs";
import { GBImageFilter } from "./gbimagefilter.mjs";

function DownloadBinary(data, name) {
  const blob = new Blob([data], { type : "application/octet-stream" });
  const dummy = document.createElement("a");
  dummy.href = URL.createObjectURL(blob);
  dummy.download = name;
  dummy.click();
  URL.revokeObjectURL(dummy.href);
}

function SetGlobalChecksum(rom) {
  const checksum_address = 0x014E;
  const checksum_length = 2;
  const head1 = 0;
  const tail1 = checksum_address;
  const head2 = tail1 + checksum_length;
  const tail2 = rom.length;
  let accum = 0;
  for (let i = head1; i < tail1; ++i) { accum += rom[i]; }
  for (let i = head2; i < tail2; ++i) { accum += rom[i]; }
  rom[checksum_address + 0] = (accum >>> 8) & 0xFF;
  rom[checksum_address + 1] = accum & 0xFF;
}

export class GBPict {
  static #resolution = [160, 144];
  #div = null;
  #wrap = document.createElement("div");
  #image_file = document.createElement("input");
  #preview = null;
  #filter = null;
  #rom_button = document.createElement("button");
  #bin_button = document.createElement("button");
  #hex_text_button = document.createElement("button");
  #base64_button = document.createElement("button");
  #textarea = document.createElement("textarea");
  
  constructor(div, init_image = null, scale = 3) {
    this.#div = div;
    this.#wrap.style.display = "flex";
    this.#image_file.type = "file";
    this.#image_file.accept = "image/*";
    this.#image_file.addEventListener("input", () => this.#InputFileCallback());
    this.#div.appendChild(this.#image_file);
    this.#div.appendChild(document.createElement("br"));
    this.#div.appendChild(this.#wrap);
    this.#preview = new ImageCanvas(this.#wrap, [GBPict.#resolution[0] * scale, GBPict.#resolution[1] * scale], init_image);
    this.#filter = new GBImageFilter(this.#wrap, GBPict.#resolution, scale);
    this.#preview.SetLoadImageCallback((img) => this.#filter.SetImage(img));
    this.#preview.SetOperationCallback((zoom, scroll) => this.#filter.SetTransform(zoom, scroll));
    this.#rom_button.innerText = "save rom";
    this.#rom_button.addEventListener("click", () => this.#SaveRom());
    this.#div.appendChild(this.#rom_button);
    this.#bin_button.innerText = "save tile";
    this.#bin_button.addEventListener("click", () => this.#SaveTileBinary());
    this.#div.appendChild(this.#bin_button);
    this.#hex_text_button.innerText = "tile hex text";
    this.#hex_text_button.addEventListener("click", () => this.#OutputHEXStr());
    this.#div.appendChild(this.#hex_text_button);
    this.#base64_button.innerText = "rom base64";
    this.#base64_button.addEventListener("click", () => this.#OutputRomBase64Str());
    this.#div.appendChild(this.#base64_button);
    this.#div.appendChild(document.createElement("br"));
    this.#textarea.style.width = GBPict.#resolution[0] * scale + "px";
    this.#textarea.style.height = "150px";
    this.#textarea.style.resize = "none";
    this.#div.appendChild(this.#textarea);
  }

  #InputFileCallback() {
    if (this.#image_file.files.length == 0) return;
    const reader = new FileReader();
    reader.onload = (event) => this.#preview.SetImageSource(event.target.result);
    reader.readAsDataURL(this.#image_file.files[0]);
  }

  async #GetRomData() {
    const bin = this.GetTileData();
    const response = await fetch("./resources/rom.gb");
    const rom_array = new Uint8Array(await response.arrayBuffer());
    const head = 0x500;
    bin.forEach((byte, address) => rom_array[head + address] = byte);
    SetGlobalChecksum(rom_array);
    return rom_array;
  }

  #SaveRom() {
    this.#GetRomData().then(rom => DownloadBinary(rom, "gbpict.gb"));
  }

  #SaveTileBinary() {
    const bin = this.GetTileData();
    DownloadBinary(bin, "tiledata.bin");
  }

  #OutputHEXStr() {
    const bin = this.GetTileData();
    let counter = 0;
    const str = bin.reduce((acum, item) => {
      const separator = counter == 15 ? "\n" : " ";
      counter = (counter + 1) % 16;
      return acum + (("00" + item.toString(16).toUpperCase()).substr(-2) + separator);
    }, "");
    this.#textarea.value = str;
  }

  #OutputRomBase64Str() {
    this.#GetRomData().then(rom => this.#textarea.value = btoa(String.fromCharCode(...rom)));
  }

  GetTilePixel() {
    const raw = this.#filter.GetPixelData();
    const tile_x = 160 / 8;
    const tile_y = 144 / 8;
    const tile_len = tile_x * tile_y;
    const buffer = new Uint8Array(raw.length);
    let counter = 0;
    for (let tile = 0; tile < tile_len; ++tile) {
      const head_x = (tile * 8) % 160;
      const head_y = Math.floor(tile / tile_x) * 8;
      for (let pixel = 0; pixel < 64; ++pixel) {
        const pixel_x = head_x + (pixel % 8);
        const pixel_y = head_y + Math.floor(pixel / 8);
        const index = pixel_x + (144 - 1 - pixel_y) * 160;
        buffer[counter++] = raw[index] < 255 / 3 * 1 ? 3 :
                            raw[index] < 255 / 3 * 2 ? 2 :
                            raw[index] < 255 / 3 * 3 ? 1 :
                            0;
      }
    }
    return buffer;
  }

  GetTileData() {
    const data = this.GetTilePixel();
    const bit_len = 8;
    const len = data.length;
    const buffer = new Uint8Array(Math.ceil(len / 4));
    let index = 0;
    let pixel = 0;
    let lo_buffer = 0;
    let hi_buffer = 0;
    for (let i = 0; i < len; ++i) {
      const bit = (bit_len - 1) - pixel;
      const lo = data[i] & 1;
      const hi = (data[i] & 2) >>> 1;
      lo_buffer |= lo << bit;
      hi_buffer |= hi << bit;
      if (++pixel >= bit_len) {
        pixel = 0;
        buffer[index++] = lo_buffer;
        buffer[index++] = hi_buffer;
        lo_buffer = 0;
        hi_buffer = 0;
      }
    }
    if (pixel != 0) {
      buffer[index++] = lo_buffer;
      buffer[index++] = hi_buffer;
    }
    return buffer;
  }

};