"use strict";

export class MouseDrag {
  #flag = false;
  #down_pos = {x : 0, y : 0};
  #buffer = {x : 0, y : 0};

  MouseDown(pos_x, pos_y, buffer_x, buffer_y) {
    this.#flag = true;
    this.#down_pos.x = pos_x;
    this.#down_pos.y = pos_y;
    this.#buffer.x = buffer_x;
    this.#buffer.y = buffer_y;
  }

  MouseUp() {
    this.#flag = false;
    this.#down_pos.x = 0;
    this.#down_pos.y = 0;
    this.#buffer.x = 0;
    this.#buffer.y = 0;
  }

  GetDistance(pos_x, pos_y) {
    if (this.#flag) {
      const dist = {x : pos_x - this.#down_pos.x, y : pos_y - this.#down_pos.y}
      const buf = {x : this.#buffer.x + dist.x, y : this.#buffer.y + dist.y}
      return {distance : dist, buffer : buf};
    } else {
      return {distance : {x : 0, y : 0}, buffer : {x : 0, y : 0}};
    }
  }

  IsDraging() { return this.#flag; }
}